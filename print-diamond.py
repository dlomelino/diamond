import sys

target = ord(sys.argv[1].upper())  # Get the command line argument and convert it to uppercase
start = ord('A')
letters = [chr(x) for x in range(start, target + 1)]  # Build our list of characters
num_letters = len(letters)
row_length = (num_letters * 2) - 1
mid = row_length / 2

for i in range(row_length):
    row = [' '] * row_length
    if i <= mid:
        lpos = mid - i
        rpos = mid + i
        row[lpos] = row[rpos] = letters[i]
    else:
        lpos = i - mid
        rpos = row_length - 1 - i + mid
        row[lpos] = row[rpos] = letters[row_length - 1 - i]
    print ''.join(row)
